package main

import (
	"bufio"
	"fmt"
	"learn/chat/tools"
	"net"
	"os"
	"sync"
)

type Client struct {
	conn net.Conn
	wg   sync.WaitGroup
}

func (client *Client) Init() {

	fmt.Println("Connexion au serveur...")

	client.initConn()

	client.wg.Add(2)

	fmt.Print("Connexion réussie !\n\n")

	client.definePseudo()

	go client.listenServer()

	go client.sendMessage()

	client.wg.Wait()

}

func (client *Client) initConn() {

	// Connexion au serveur
	conn, err := net.Dial("tcp", fmt.Sprintf("%s:%s", tools.IP, tools.PORT))

	tools.GestionErreur(err)

	client.conn = conn

}

func (client *Client) definePseudo() {

	for {

		fmt.Println("Quel est votre pseudo (entre 3 et 20 caractères) :")

		pseudo := readInput()

		pseudoLen := len(pseudo)

		if pseudoLen <= 20 && pseudoLen >= 3 {

			client.conn.Write([]byte(pseudo))

			break

		}

		fmt.Print("Pseudo incorrecte\n\n")

	}

	fmt.Println("")

}

func (client *Client) sendMessage() {

	defer client.wg.Done()

	for {

		text := readInput()

		client.conn.Write([]byte(text))

	}

}

func readInput() string {

	reader := bufio.NewReader(os.Stdin)

	text, err := reader.ReadString('\n')

	tools.GestionErreur(err)

	return text

}

func (client *Client) listenServer() { // goroutine dédiée à la reception des messages du serveur

	defer client.wg.Done()

	for {

		message, err := bufio.NewReader(client.conn).ReadString('\n')

		tools.GestionErreur(err)

		fmt.Print(message, "\n")

	}
}

func main() {

	client := Client{}

	client.Init()

}
