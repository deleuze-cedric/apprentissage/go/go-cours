package main

import (
	"bufio"
	"fmt"
	"io"
	"learn/chat/tools"
	"net"
	"os"
	"strings"
	"time"
)

type Server struct {
	listener         net.Listener
	logFile          *os.File
	mw               io.Writer
	connectedClients map[string]net.Conn
}

func (server *Server) Init() {

	fmt.Println("Lancement du serveur ...")

	server.initListener()

	server.initLogFile()

	server.connectedClients = make(map[string]net.Conn)

	fmt.Print("Serveur prêt !\n\n")

}

func (server *Server) initListener() {

	ln, err := net.Listen("tcp", fmt.Sprintf("%s:%s", tools.IP, tools.PORT))

	tools.GestionErreur(err)

	server.listener = ln

}

func (server *Server) initLogFile() {

	server.logFile.Close()

	date := time.Now()

	fileName := date.Format("2006-01-02T15:04:05") + ".log"

	file, err := os.OpenFile("/"+fileName, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0611)

	tools.GestionErreur(err)

	server.logFile = file

	server.mw = io.MultiWriter(os.Stdout, server.logFile)

}

func (server *Server) waitClients() {

	for {

		conn, err := server.listener.Accept()

		if err == nil {

			username, err := server.getUsername(conn)

			if err == nil {

				server.connectedClients[username] = conn

				tools.GestionErreur(err)

				loginMessage := fmt.Sprint(username, " s'est connecté")

				fmt.Fprint(server.mw, loginMessage, " depuis ", conn.RemoteAddr(), "\n\n")

				server.alertOtherClient(conn, loginMessage, true)

				go server.listenClient(username, conn)

			}

		}
	}
}

func (server *Server) listenClient(username string, conn net.Conn) {

	buf := bufio.NewReader(conn)

	for {

		messageSend, err := buf.ReadString('\n')

		if err != nil {

			delete(server.connectedClients, username)

			message := fmt.Sprint(username + " s'est déconnecté.\n")

			fmt.Fprintf(server.mw, message, "\n")

			server.alertOtherClient(conn, message, true)

			break

		}

		server.alertOtherClient(conn, messageSend, false)

	}
}

func (server *Server) getUsername(conn net.Conn) (string, error) {

	buf := bufio.NewReader(conn)

	for {

		username, err := buf.ReadString('\n')

		username = strings.Trim(username, "\n")

		if err != nil {

			fmt.Fprintf(server.mw, "Client disconnected during setting username process.\n\n")

			return username, err

		}

		_, usernameExists := server.connectedClients[username]

		if !usernameExists {

			sendMessageFromServer(conn, ("Bienvenue sur le chat, vous pouvez maitenant conversez avec les autres utilisateurs"))

			return username, nil

		}

		sendMessageFromServer(conn, fmt.Sprintf("Le pseudo %s est déjà pris\n", username))

	}

}

func (server *Server) getUsernameFromConnection(conn net.Conn) string {

	for username, iteratedConn := range server.connectedClients {

		if conn == iteratedConn {

			return username

		}

	}

	return ""

}

func (server *Server) alertOtherClient(conn net.Conn, message string, fromServer bool) {

	username := server.getUsernameFromConnection(conn)

	for _, c := range server.connectedClients {

		if c != conn {

			if !fromServer {

				c.Write([]byte(username + " : " + message + "\n"))

			} else {

				sendMessageFromServer(conn, message)

			}

		}

	}

}

func sendMessageFromServer(conn net.Conn, message string) {

	conn.Write([]byte(fmt.Sprintln("Server : " + message)))

}

func (server *Server) Close() {

	fmt.Println("Extinction du serveur...")

	server.logFile.Close()

}

func main() {

	server := Server{}

	server.Init()

	defer server.Close()

	server.waitClients()

}
