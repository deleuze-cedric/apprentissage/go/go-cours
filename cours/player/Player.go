package player

import (
	"fmt"
	"time"
)

type Player struct {
	Year int

	Month int

	Day int

	Name string
}

func (player Player) String() string {

	monthStruc := time.Month(player.Month)

	var birthdate, now time.Time = time.Date(player.Year, monthStruc, player.Day, 0, 0, 0, 0, time.UTC), time.Now()

	var age time.Duration = now.Sub(birthdate)

	return fmt.Sprintf("Le/la joueur.euse nommé.e '%s' est né.e le %d %s %d. Il/Elle a donc %d ans.\n", player.Name, player.Day, monthStruc, player.Year, int(age.Hours()/24/365))

}
