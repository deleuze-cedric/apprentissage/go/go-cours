package main

import (
	"bufio"
	"fmt"
	"learn/cours/player"
	"learn/utils"
	"os"
	"strconv"
	"strings"
	"time"
)

var now = time.Now()
var currentYear = now.Year()
var currentMonth = now.Month()
var maxNbPlayer = 5

func main() {

	fmt.Println("Salut !")

	var scanner utils.MyScanner = utils.MyScanner{Scanner: bufio.NewScanner(os.Stdin)}

	nbPlayer := scanner.PrintAndScanInt("Combien de joueur.euse.s veulent se présenter au tournoi ? (max "+strconv.Itoa(maxNbPlayer)+")", validateNumberOFPlayer)

	var players = make([]player.Player, nbPlayer)

	for playerIndex := range players {

		fmt.Println("\nJoueur.se", playerIndex+1)

		var name string

		for true {

			fmt.Println("\nQuel est son prénom ? ")

			scanner.Scan()

			name = strings.TrimSpace(scanner.Text())

			if name == "" {

				fmt.Println("Veuillez renseigner un prénom valide !")

			} else if len(name) < 2 {

				fmt.Println("Un prénom comporte au moins 2 caractères !")

			} else {

				break

			}

		}

		year := scanner.PrintAndScanInt("En quelle année est il/elle né.e ?", validateYear)

		month := scanner.PrintAndScanInt("Quel mois ?", func(month int) bool { return validateMonth(year, month) })

		day := scanner.PrintAndScanInt("Quel jour ?", func(day int) bool { return validateDay(year, month, day) })

		players[playerIndex] = player.Player{
			Year:  year,
			Month: month,
			Day:   day,
			Name:  name,
		}

		fmt.Println("\n-----------------------------------------")

	}

	fmt.Println(players)

	for _, player := range players {

		fmt.Println(player)

	}

}

func validateNumberOFPlayer(nbPlayer int) bool {

	switch {

	case nbPlayer < 1:
		fmt.Println("Il faut au moins 1 joueur !")

	case nbPlayer > maxNbPlayer:
		fmt.Println("Maximum", maxNbPlayer, " joueurs !")

	default:
		return true

	}

	return false

}

func validateYear(year int) bool {

	switch {

	case year > currentYear:
		fmt.Println("Reste dans le présent !")

	case year < currentYear-123:
		fmt.Println("Impossible ! Tu serais plus âgé.e que Jean Louise Calment ! ")

	default:
		return true

	}

	return false

}

func validateMonth(year int, month int) bool {

	switch {

	case year == currentYear && month > int(currentMonth):
		fmt.Println("Trop dans le turfu mon grand !")

	case month < 1 || month > 12:
		fmt.Println("Ton mois de naissance est forcément entre 1 et 12...")

	default:
		return true

	}

	return false

}

func validateDay(year int, month int, day int) bool {

	monthStruc := time.Month(month)

	nbDayInMonth := time.Date(year, monthStruc+1, 0, 0, 0, 0, 0, time.UTC).Day()

	if day < 1 || day > nbDayInMonth {

		fmt.Printf("Le mois de ta naissance commence le 1 et finit le %d\n", nbDayInMonth)

	} else {

		date := time.Date(year, monthStruc, day, 0, 0, 0, 0, time.UTC)

		if date.After(date) {

			fmt.Println("Il semblerait que tu sois né.e dans le futur...")

		} else {

			return true

		}

	}

	return false

}
