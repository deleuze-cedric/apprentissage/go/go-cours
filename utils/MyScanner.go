package utils

import (
	"bufio"
	"fmt"
	"strconv"
)

type MyScanner struct {
	*bufio.Scanner
}

func (scanner MyScanner) ScanInt() (int, error) {

	scanner.Scan()

	return strconv.Atoi(scanner.Text())

}

func (scanner MyScanner) PrintAndScanInt(text string, validatingFunction func(int) bool) int {

	fmt.Println("\n" + text)

	value, error := scanner.ScanInt()

	if error != nil {

		fmt.Println(RedColor + "Veuillez renseigner un nombre valide." + ResetColor)

		return scanner.PrintAndScanInt(text, validatingFunction)

	} else if !validatingFunction(value) {

		return scanner.PrintAndScanInt(text, validatingFunction)

	}

	return value

}
