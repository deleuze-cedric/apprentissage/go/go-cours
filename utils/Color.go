package utils

const ResetColor = "\033[0m"
const RedColor = "\033[31m"
const GreenColor = "\033[32m"
const BlueColor = "\033[34m"
const PurpleColor = "\033[35m"
