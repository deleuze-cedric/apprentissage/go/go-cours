package main

import (
	"bufio"
	"fmt"
	"io"
	"learn/utils"
	"os"
	"strconv"
	"sync"
	"time"
)

const (
	// max 15810*15810
	nbRow              = 15810
	nbColl             = 15810
	nbAlignedCellToWin = 3
	nbCell             = nbRow * nbColl
	nbPlayer           = 2 // Si plus de 2 joueurs, on joue en équipe
)

var playersMoves [nbRow][nbColl]int

func main() {

	var winner int = 0

	var turn int = 1

	var scanner utils.MyScanner = utils.MyScanner{Scanner: bufio.NewScanner(os.Stdin)}

	date := time.Now()

	fileName := "party_" + date.Format("2006-01-02T15:04:05") + ".score"

	file, err := os.OpenFile("./"+fileName, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0711)

	mw := io.MultiWriter(os.Stdout, file)

	defer file.Close()

	if err != nil {
		panic("Impossible de créer/lire le fichier : " + err.Error())
	}

	for winner == 0 && turn <= nbCell {

		for player := 1; player <= nbPlayer; player++ {

			team := definePlayerTeam(player)

			turnMessage := fmt.Sprintf("\nTour %d : Joueur n°%d\n", turn, player)
			fmt.Fprintln(mw, turnMessage)

			grid := stringGrid()
			fmt.Fprintln(mw, grid)

			question := "\nSur quelle case voulez-vous jouer ? "
			file.WriteString(question + "\n")
			chosenCell := scanner.PrintAndScanInt(question, validateChosenCell)
			file.WriteString(strconv.Itoa(chosenCell) + "\n")

			chosenRow, chosenCol := defineRowAndColFromCell(chosenCell)

			playersMoves[chosenRow][chosenCol] = player

			if checkIfTeamHasWin(team, true) {

				winner = team

				break

			}

			turn++

			if turn > nbCell {

				break

			}

		}

	}

	str := "\n---------------------------------------------------------------------------------\n\n" + stringGrid() + "\n"

	if winner != 0 {

		str += utils.GreenColor + "Bravo équipe n° " + strconv.Itoa(winner) + " vous avez gagnez !" + utils.ResetColor

	} else {

		str += utils.PurpleColor + "Malheureusement personne n'a gagné..." + utils.ResetColor

	}

	fmt.Fprintln(mw, str)

}

func stringGrid() string {

	if nbRow*nbColl >= 100 {
		return "Trop de possibilités à afficher..."
	}

	str := ""

	for rowIndex := range playersMoves {

		for collIndex := range playersMoves[rowIndex] {

			cellNumber := (rowIndex * nbColl) + (collIndex + 1)
			value := strconv.Itoa(cellNumber)

			if cellNumber < 10 {
				value = "0" + value
			}

			player := playersMoves[rowIndex][collIndex]

			if player != 0 {

				team := definePlayerTeam(player)
				value = " X"

				if team == 0 {

					value = " O"

				}

			}

			str += value + " "

		}

		str += "\n"

	}

	return str

}

func definePlayerTeam(player int) int {

	if player == 0 {

		return -1

	}

	return player % 2

}

func checkIfTeamHasWin(teamToCheck int, goRoutineMode bool) bool {

	debut := time.Now()

	defer fmt.Println("Temps de traitement pour définir si l'équipe a gagné :", time.Now().Sub(debut))

	winning := false

	wg := &sync.WaitGroup{}

	ch := make(chan bool)

	var nbRoutineOpen, nbRoutineProcessed int

	for rowIndex := range playersMoves {

		for collIndex, player := range playersMoves[rowIndex] {

			playerTeam := definePlayerTeam(player)

			if player != 0 && playerTeam == teamToCheck {

				var endRowIndex = rowIndex + nbAlignedCellToWin

				var endCollIndex = collIndex + nbAlignedCellToWin

				var startCollIndex = collIndex - nbAlignedCellToWin + 1

				wg.Add(4)
				nbRoutineOpen += 4

				if goRoutineMode {

					go goCheckTeamIsWinningOnHorizontalLine(wg, ch, teamToCheck, rowIndex, collIndex, endCollIndex)
					go goCheckTeamIsWinningOnVerticalLine(wg, ch, teamToCheck, rowIndex, endRowIndex, collIndex)
					go goCheckTeamIsWinningOnLeftDiagonalLine(wg, ch, teamToCheck, rowIndex, endRowIndex, startCollIndex, collIndex)
					go goCheckTeamIsWinningOnRightDiagonalLine(wg, ch, teamToCheck, rowIndex, endRowIndex, collIndex, endCollIndex)

				} else {

					if checkTeamIsWinningOnHorizontalLine(teamToCheck, rowIndex, collIndex, endCollIndex) ||
						checkTeamIsWinningOnVerticalLine(teamToCheck, rowIndex, endRowIndex, collIndex) ||
						checkTeamIsWinningOnLeftDiagonalLine(teamToCheck, rowIndex, endRowIndex, startCollIndex, collIndex) ||
						checkTeamIsWinningOnRightDiagonalLine(teamToCheck, rowIndex, endRowIndex, collIndex, endCollIndex) {
						return true
					}

				}

			}

		}

	}

	if goRoutineMode {

		for isWinning := range ch {

			winning = isWinning || winning

			nbRoutineProcessed++

			if nbRoutineOpen == nbRoutineProcessed {
				close(ch)
			}

		}

		wg.Wait()

	}

	return winning

}

func goCheckTeamIsWinningOnHorizontalLine(wg *sync.WaitGroup, ch chan bool, team, rowIndex, startCollIndex, endCollIndex int) {

	defer wg.Done()

	ch <- checkTeamIsWinningOnHorizontalLine(team, rowIndex, startCollIndex, endCollIndex)

}

func checkTeamIsWinningOnHorizontalLine(team, rowIndex, startCollIndex, endCollIndex int) bool {

	if endCollIndex > nbColl {

		return false

	}

	for _, iteratedPlayer := range playersMoves[rowIndex][startCollIndex:endCollIndex] {

		iteratedPlayerTeam := definePlayerTeam(iteratedPlayer)

		if iteratedPlayerTeam != team {

			return false

		}

	}

	return true
}

func goCheckTeamIsWinningOnVerticalLine(wg *sync.WaitGroup, ch chan bool, team, startRowIndex, endRowIndex, collIndex int) {

	defer wg.Done()

	ch <- checkTeamIsWinningOnVerticalLine(team, startRowIndex, endRowIndex, collIndex)
}

func checkTeamIsWinningOnVerticalLine(team, startRowIndex, endRowIndex, collIndex int) bool {

	if endRowIndex > nbRow {

		return false

	}

	for _, filteredPlayerMoves := range playersMoves[startRowIndex:endRowIndex] {

		iteratedPlayer := filteredPlayerMoves[collIndex]

		iteratedPlayerTeam := definePlayerTeam(iteratedPlayer)

		if iteratedPlayerTeam != team {

			return false

		}

	}

	return true
}
func goCheckTeamIsWinningOnLeftDiagonalLine(wg *sync.WaitGroup, ch chan bool, team, startRowIndex, endRowIndex, startCollIndex, endCollIndex int) {

	defer wg.Done()

	ch <- checkTeamIsWinningOnLeftDiagonalLine(team, startRowIndex, endRowIndex, startCollIndex, endCollIndex)

}

func checkTeamIsWinningOnLeftDiagonalLine(team, startRowIndex, endRowIndex, startCollIndex, endCollIndex int) bool {

	if endRowIndex > nbRow || startCollIndex < 0 {

		return false

	}

	for currentRowIndex, rowValues := range playersMoves[startRowIndex:endRowIndex] {

		for currentCollIndex, iteratedPlayer := range rowValues[startCollIndex : endCollIndex+1] {

			if nbAlignedCellToWin-currentRowIndex == currentCollIndex+1 {

				iteratedPlayerTeam := definePlayerTeam(iteratedPlayer)

				if iteratedPlayerTeam != team {

					return false

				}

			}

		}

	}

	return true

}

func goCheckTeamIsWinningOnRightDiagonalLine(wg *sync.WaitGroup, ch chan bool, team, startRowIndex, endRowIndex, startCollIndex, endCollIndex int) {

	defer wg.Done()

	ch <- checkTeamIsWinningOnRightDiagonalLine(team, startRowIndex, endRowIndex, startCollIndex, endCollIndex)

}

func checkTeamIsWinningOnRightDiagonalLine(team, startRowIndex, endRowIndex, startCollIndex, endCollIndex int) bool {

	if endRowIndex > nbRow || endCollIndex > nbColl {

		return false

	}

	for currentRowIndex, filteredPlayerMoves := range playersMoves[startRowIndex:endRowIndex] {

		for currentCollIndex, iteratedPlayer := range filteredPlayerMoves[startCollIndex:endCollIndex] {

			if currentRowIndex == currentCollIndex {

				iteratedPlayerTeam := definePlayerTeam(iteratedPlayer)

				if iteratedPlayerTeam != team {

					return false

				}

			}

		}

	}

	return true

}

func defineRowAndColFromCell(cell int) (int, int) {

	cell--

	chosenRow := cell / nbColl

	chosenCol := cell % nbColl

	return chosenRow, chosenCol

}

func validateChosenCell(chosenCell int) bool {

	chosenRow, chosenCol := defineRowAndColFromCell(chosenCell)

	if chosenCell < 1 || chosenCell > nbCell {

		fmt.Println(utils.RedColor + "Veuillez choisir une case entre 1 et " + strconv.Itoa(nbCell) + utils.ResetColor)

	} else if playersMoves[chosenRow][chosenCol] != 0 {

		fmt.Println(utils.BlueColor + "Case déjà jouée..." + utils.ResetColor)

	} else {

		return true

	}

	return false

}
